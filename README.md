# Artificial Dilution Series

### Abstract 
GO classifiers and other methods for automatic annotations of novel sequences play an important role in modern biosciences. It is thus important to assess the quality of different GO classifiers. This evaluation depends heavily on the used evaluation metrics as they define how methods will be ranked in the evaluation. However, little research has been done on evaluation metrics. Indeed most  evaluation metrics are simply borrowed from machine learning without any testing for their applicability to GO classification. 

We propose a novel simple comparison of metrics, called Artificial Dilution Series. We start by selecting a set of annotations that are known a priori to be correct. From this set we create multiple copies and introduce different amount of errors to each copy. This creates a "series" of annotation sets with the percentage of original correct annotations ("signal") decreasing from one end of the series to the other. Next we test metrics to see which of them are good at separating annotation sets at different signal levels. In addition, we test metrics with various false positive annotation sets, and show where they rank in the generated signal range.

We compared a large set of evaluation metrics with ADS, revealing drastic differences between them. Especially we show how some metrics consider false positive datasets as good as 100 % correct data sets and how some metrics perform poorly at separating the different error levels. This work A) shows that evaluation metrics should be tested for their performance; B) presents a software that can be used to test different metrics on real-life datasets; C) gives guide lines on what evaluation metrics perform well with Gene Ontology structure D) proposes improved versions for some well-known evaluation metrics. The presented methods are also applicable to other areas of science where evaluation of prediction results is non-trivial.

### ADS Pipeline
C++, Perl and R code for Artificial Dilution Series (ADS) pipeline. ADS pipeline takes in a data set of protein/gene identifiers annotated with Gene Ontology (GO) classes. The pipeline then creates multiple copies of this original data set with varying percentage of erroneous annotations. When these data sets are arranged in order of the decreasing percentage of original annotations, this can be viewed as a "dilution series" of the original signal. 

The main entry point to the analysis is the pipeline.pl script. All scripts/executables have an online manual that can be accessed by the "-h" flag, e.g. type ./pipeline.pl -h to get started.

ADS user manual including example pipeline runs is available here:
http://ekhidna2.biocenter.helsinki.fi/ADS/


