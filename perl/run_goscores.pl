#! /usr/bin/perl
use strict;
use warnings;

#
# RUN GOSCORES ON A SET OF FILES
#

# 

my $GOSCORES= "/data/ilja/go/cpp/v2.3/goscores";
my $data_pos= "/data/petrit/GOA_annotations/Ilja_GO_project/TmpOutput/test.GO_data.cleaned.subset1000";
my $data_pred_dir= "/data/petrit/GOA_annotations/Ilja_GO_project/OutputFiles2"; 
my $K= 40;
my $C= 10;


for(my $ci=0; $ci<1; $ci++){
	for(my $pi=0; $pi<$K; $pi++){
		
		my $data_pred= "$data_pred_dir/test.GO_data.cleaned.1000.elab.c$ci.GO-dists_$pi.Data";
		my $call= "$GOSCORES -p $data_pred -t $data_pos -m LIST=go,TH=simple,SUMF=mean,SF=AUC";
		
		#print "$data_pred\n";
		#print "$call\n";
		
		print "$ci:$pi:\n";
		if(system($call)!=0){
			print "ERROR: error cat,permutation: $ci,$pi\n";
		}
	}
}
