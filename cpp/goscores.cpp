#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <unordered_map>
#include <cstring>
#include <algorithm>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

// OTHER FILES
#include <golib.h>
#include <gograph.h>

using namespace std;


struct parameters{
	char* prog_name;
	char* goa_pred;
	char* goa_pos;
	char* obo_file;
	char* ic_file;
	char* stats_file;
	string mode;
	bool propagate_parents;
	bool verbal;
};
struct score_parameters{	
	string list;	// type of lists that are compared: GO-lists or gene-lists
	string thf; 	// thresholding function
	string sumf1;	// summary function to summarise score accross genes/GOs
	string sumf2;	// summary function to summarise score accross thresholds
	string scoref;	// similarity score between two lists
	string simf;	// semantic similarity
	unsigned int minClassSize;	// Minimun size for GO class in Positive Set.
	unsigned int goclass_count;	// Number of uniq GO classes (in database or ontology, set by upper stream calls).
	
	score_parameters(string _list="NULL",
			string _thf="NULL",
			string _sumf1="NULL",
			string _sumf2="NULL",
			string _scoref="NULL",
			string _simf="NULL",
			unsigned int _classSize=10,
			unsigned int _goclass_count=0):
	list(_list),thf(_thf),sumf1(_sumf1),sumf2(_sumf2),scoref(_scoref),simf(_simf),minClassSize(_classSize),goclass_count(_goclass_count){};
};
void print_score_parameters(score_parameters score_pars){
	cout 	<< "\tLIST\t: "<< score_pars.list <<"\n"
		<< "\tTHF \t: "<< score_pars.thf <<"\n"
		<< "\tSUMF1\t: "<< score_pars.sumf1 <<"\n"
		<< "\tSUMF2\t: "<<score_pars.sumf2<<"\n"
		<< "\tSF\t: "<<score_pars.scoref<<"\n"
		<< "\tSIMF\t: "<<score_pars.simf<<"\n"
		<< "\tMINCS\t: "<<score_pars.minClassSize<<"\n"
		<< "\tGOC\t: "<<score_pars.goclass_count<<"\n";
}

/* PT modified generate scores 06.12.2018

   I created read_go_truth_annotation function

   This does not require having the score values in the 
   GO annotation table. 
   In my tests it seems to work with and without score values
*/
void generate_scores(const parameters pars, score_parameters score_pars){

	// READING DATA
	vector<go_annotation> data_pred= read_go_annotation_data(pars.goa_pred);
	vector<go_annotation> data_pos = read_go_truth_annotation(pars.goa_pos);
	//vector<go_annotation> data_pos = read_go_annotation_data(pars.goa_pos);
	
	// DEBUGGING COMMAND BELOW	
	// print_go_annotation_data(data_pos);

	// SANITY CHECK
	double min_score	= get_min_score(data_pred);
	double max_score	= get_max_score(data_pred);
	if( !(min_score < max_score) ){
		cerr << "ERROR: check data scores: min_score(data) < max_score(data) must hold\n";
		exit(1);
	}
	
	go_graph graph;
	if(pars.obo_file != NULL){
		//cout << "# reading obo file\n"<<flush;
		vector<go_node> go_nodes = read_obo_file(pars.obo_file, true); 
		graph= create_go_graph(go_nodes);
	}
	//unsigned int go_count_ontology= graph.nodes.size();
	
	unordered_map<unsigned int,vector<unsigned int> > pmap;
	if(pars.obo_file != NULL){
		//cout << "# searching for GO parents\n"<<flush;
		pmap = get_parent_map(graph,"isa,partof,consider,replacedby,altid",true,true,true);
	}
	
	unordered_map<unsigned int,double> icmap;
	if(pars.ic_file != NULL){
		//cout << "# reading information content\n";
		read_information_content(pars.ic_file,icmap);
	}
	
	// propagate parents
	if(pars.propagate_parents){
		propagate_parents(data_pos, pmap);
		propagate_parents(data_pred,pmap);
	}
	
	
	// SCORE PRED AGAINST POS
	vector<double> score_list;
	unordered_map<string,double> score_map;
	double score = 0;

	if(score_pars.scoref == "Fmax"){
		score = get_score_cafa_fmax(data_pred,data_pos);
	}
	else if(score_pars.scoref == "AUC"){
		score = get_AUC(data_pred,data_pos,score_pars.minClassSize, pars.verbal);
	}
	else if(score_pars.scoref == "Smin1"){
		score_list = get_score_cafa_smin_PT(data_pred,data_pos,icmap);
		score = score_list[0];
	}
	else if(score_pars.scoref == "Smin2"){
		score_list = get_score_cafa_smin_PT(data_pred,data_pos,icmap);
		score = score_list[1];
	}
	else if(score_pars.scoref == "Smin3"){
		score_list = get_score_cafa_smin_PT(data_pred,data_pos,icmap);
		score = score_list[2];
	}
	else if(score_pars.scoref == "SimGIC"){
		score_list = get_score_cafa_smin_PT(data_pred,data_pos,icmap);
		score = score_list[3];
	}
	else if(score_pars.scoref == "SimGIC2"){
		score_list = get_score_cafa_smin_PT(data_pred,data_pos,icmap);
		score = score_list[4];
	}	
	else if(score_pars.scoref == "SminAll"){ 
		score_list = get_score_cafa_smin_PT(data_pred,data_pos,icmap);
	}	
	else if(score_pars.list == "go"){
		if(score_pars.thf == "all"){
			score_map= get_score_bygene(data_pred,
							data_pos,
							pmap,
							icmap,
							score_pars.scoref,
							score_pars.simf,
							score_pars.goclass_count,
							pars.verbal);
			score_list= get_values(score_map);
		}
		else if(score_pars.thf == "th"){
			score_list= get_score_bygene_th(data_pred,
							data_pos,
							pmap,
							icmap,
							score_pars.scoref,
							score_pars.simf,
							score_pars.sumf1,
							pars.verbal);
		}
		else{
			cerr<<"ERROR: generate_scores(): invalid TH:"<<score_pars.thf<<"\n";
			exit(1);
		}
		// summarising scores to a single value
			// across genes with SUMF1
		if(score_pars.thf=="all"){
			if(score_pars.sumf1=="max")
				score = max(score_list);
			else if(score_pars.sumf1=="mean")
				score = mean(score_list);
			else if(score_pars.sumf1=="median")
				score = median(score_list);
		}
			// across thresholds with SUMF2
		else if(score_pars.thf =="th"){
			if(score_pars.sumf2=="max")
				score = max(score_list);
			else if(score_pars.sumf2=="mean")
				score = mean(score_list);
			else if(score_pars.sumf2=="median")
				score = median(score_list);
		}		
	}
			
	else if(score_pars.list == "gene"){
		score_list= get_score_bygo(data_pred,
					data_pos,
					score_pars.scoref,
					score_pars.sumf1,
					score_pars.minClassSize,
					pars.verbal);
		if(score_pars.sumf1 == "max")
			score = max(score_list);		
		else if(score_pars.sumf1 == "mean")
			score = mean(score_list);
		else if(score_pars.sumf1 == "median")
			score = median(score_list);					
	}
	else if(score_pars.list=="gene_go"){
		score = get_score_singlelist(data_pred,
					data_pos,
					score_pars.scoref,
					score_pars.thf,
					score_pars.sumf1,
					score_pars.goclass_count);
	}
	
	// PRINT SCORES
	FILE * OUT= stdout;
	if(pars.stats_file != NULL){
		OUT = fopen(pars.stats_file,"w");
	}
	if(score_pars.scoref == "SminAll"){
		// Dirty implementation for printing score_list
		fprintf(OUT,"%1.6f\t",score_list[0]);
		fprintf(OUT,"%1.6f\t",score_list[1]);
		fprintf(OUT,"%1.6f\t",score_list[2]);
		fprintf(OUT,"%1.6f\t",score_list[3]);
		fprintf(OUT,"%1.6f\n",score_list[4]);
	}
	else{
		fprintf(OUT,"%1.6f\n",score);
	}
}


void print_usage_short(char* name){
	cerr << "USAGE: "<< name <<" -g -p pred -t pos -b obo_file -i ic_file -o output -vh -m LIST=str,TH=str,SUMF1=str,SUMF2=str,SF=str,SIMF=str,MINCS=int,GOC=int\n\n"
		<<"-h      \t: print help manual\n";
}
		
void print_usage_long(char* name){
	cerr << "# goscores v1.0: scoring Gene Onthology Annotation files\n"
		<< "# credits and citing:\n"
		<< "#\n"
		<< "# Plyusnin I., Holm L., Toronen P. Novel Comparison of Evaluation Metrics for Gene Ontology Classifiers Reveals Drastic Performance Differences //bioRxiv. 2018\n"
		<< "#\n"
		<< "# contact: Petri.Toronen@helsinki.fi\n"
		<< "\n"
		<< "USAGE: "<< name <<" -p pred -t pos -b obo_file -i ic_file -o output -g -vh -m LIST=str,TH=str,SUMF1=str,SUMF2=str,SF=str,SIMF=str,MINCS=int,GOC=int\n\n"
		<< "-p file \t: predicted GO annotation [gene\\tgo\\tscore]\n"
		<< "-t file \t: true GO annotation [gene\\tgo\\tscore]\n"
		<< "-b file \t: OBO 1.2 file\n"
		<< "-i file \t: information content file [GO\\tIC]\n"
		<< "-o file \t: output file\n"
		<< "-g [off]\t: propagate parents in pred and pos data\n"
		<< "-v [off]\t: verbal mode\n"
		<< "-h      \t: print this help manual\n"
		
		<< "-m str [data]\t: output mode/evaluation method, valid options:\n"
		<< "   data      \t: permuted data\n"
		<< "   LIST=str,TH=str,SUMF1=str,SUMF2=str,SF=str,SIMF=str,MINCS=int,GOC=int\n"
		<< "             \t: parameters defining evaluation of permuted data(predicted) against positive\n\n"
		
		<< "      LIST=[go]   evaluate:\n"
		<< "            gene   : gene-lists for each go\n"
		<< "            go     : go-lists for each gene\n"
		<< "            gene_go: single-list of gene-go pairs\n\n"
		
		<< "      TH=[all]  threshold pred scores with options:\n"
		<< "            all    : for each gene evaluates ALL predictions against positives (no theshold)\n"
		<< "                     summarize scores across genes with SUMF1\n"
		<< "            geneth : for each gene evaluates predictions of all possible lengths (1st, 1st & 2nd, .., all)\n"
		<< "                     summarize scores first across thresholds (SUMF2), then across genes (SUMF1)\n"
		<< "            th     : like in CAFA I thresholds all data at stepwise increasing thresholds\n"
		<< "                     summarize scores first across genes (SUMF1), then across thresholds (SUMF2)\n\n"
		
		<< "      SUMF1=[mean] summarize scores across genes/gos with:\n"
		<< "            max|mean|median\n\n"
		
		<< "      SUMF2=[mean] summarize scores across thresholds with:\n"
		<< "            max|mean|median\n\n"
		     
		<< "      SF=[AUC1]     score list of predicted gos/genes agains positives with:\n"
		<< "            U       : Mann-Whitney U-statistic\n"
		<< "            AUC     : AUC with golib.h:get_AUC()\n"		
		<< "            AUC1    : area under ROC curve, scaled by (|TP||FN|) with pseudocounts\n"
		<< "            AUC2    : area under ROC curve, scaled by (|P||FN|) with pseudocounts\n"
		<< "            AUC3    : area under ROC curve, scaled by (|P||N|), |N| defined as:\n"
		<< "              US AUC3 : |N|         = |uniq_genes(pos)| x GOC - |pos|\n"
		<< "              GC AUC3 : |N(gene X)| = GOC - |pos(gene X)|\n"
		<< "              TC AUC3 : |N(term Y)| = |uniq_genes(pos)| - |pos(term Y)|\n"
		<< "            AUCPR   : auc by trapezoidal rule, under Precision Recall curve (not ROC)\n"
		<< "            Jacc    : JaccardIndex(pred,pos)\n"
		<< "            wJacc   : Jaccard index weighted by information content\n"
		<< "            Fmax    : Fmax metric (CAFA I & II)\n"
		
		<< "            Smin1   : Smin metric (CAFA II)\n"
		<< "            Smin2   : gene-centric variant\n"
		<< "            Smin3   : another variant (see S2-Text)\n"
		<< "            SimGIC  : IC weighted Jaccard (gene-centric)\n"
		<< "            SimGIC2 : IC weighted Jaccard (unstructured)\n"
		<< "            SminAll : vector containing all above Smin and SimGIC scores\n"		

		<< "            score_A : mean( sim_mat*(pred,pos) )\n"
		<< "            score_B : mean( col_max(sim_mat(pred,pos)) )\n"
		<< "            score_C : mean( row_max(sim_mat(pred,pos)) )\n"
		<< "            score_D : mean( score_B,score_C)\n"
		<< "            score_E : min( score_B,score_C)\n"
		<< "            score_F : mean( pooled(col_max(sim_mat),row_max(sim_mat)) )\n\n"
		<< "            *similarity matrix with rows for predictions and columns for positives\n\n"
		
		<< "      SIMF  compute similarity matrix with semantic similarity:\n"
		<< "            pJacc   : JaccardIndex( parents(pred),parents(pos) )\n"
		<< "            Resnik  : Resnik (1995)\n"
		<< "            Lin     : Lin (1998)\n"
		<< "            PathLin : Koskinen et al(2014)\n"
		<< "            JC      : Jiang & Conrath (1997)\n"
		<< "            id      : identity function (baseline)\n\n"
		
		<< "      MINCS Minimum size of GO class in Positive Set. Default to 10\n"
		<< "      GOC   Number of uniq GO classes (applied to AUC metrics)\n\n";		
}

int main (int argc, char** argv) {


	if(argc<2){
		print_usage_short(argv[0]);
		exit(0);
	}

	// INPUT PARAMETERS
	parameters pars;
	pars.prog_name= 	argv[0];
	pars.goa_pred= 	NULL;
	pars.goa_pos= 	NULL;
	pars.obo_file = 	NULL;
	pars.ic_file  =		NULL;
	pars.stats_file = 	NULL;
	pars.mode = 		"LIST=go,SUMF=mean,SF=AUC";
	pars.propagate_parents= false;
	pars.verbal= 		false;
	
	score_parameters score_pars("go","all","mean","mean","AUC1","NULL",10,46316);
	
	int option= 0;
	while ((option = getopt(argc, argv,"hp:t:b:i:o:gvm:")) != -1) {
        switch (option) {
		case 'h':
			print_usage_long(pars.prog_name);
			exit(0);			
		case 'p':
			pars.goa_pred= optarg; 
                	break;
		case 't':
			pars.goa_pos= optarg;
			break;
		case 'b' :
	     		pars.obo_file= optarg;
			break;
		case 'i' :
			pars.ic_file= optarg;
			break;
 		case 'o' :
	     		pars.stats_file= optarg;
                	break;
		case 'g':
			pars.propagate_parents= true;
			break;
		case 'v':
			pars.verbal= true;
			break;
		case 'm':
			pars.mode= string(optarg);
			break;			
		case '?':
        		cerr << "option -"<<optopt<<" requires an argument\n";
        		exit(1);
             	default:
	     		print_usage_short(pars.prog_name); 
                	exit(1);
        	}
    	}
	// PARSING MODE PARAMETERS
	vector<string> pair_list= split(pars.mode,",");
	for(unsigned int i=0; i<pair_list.size(); i++){
		vector<string> pair= split(pair_list[i],"=");
		if(pair.size() != 2){
			cerr << "ERROR: invalid -m argument:\""<<pair_list[i]<<"\"\n";
			exit(1);
		}
		if(pair[0] == "LIST" && (pair[1]=="gene" || pair[1]=="go" || pair[1]=="gene_go")){
			score_pars.list=pair[1];
		}
		else if(pair[0] == "TH" && (pair[1]=="all"
						|| pair[1]=="th")){
			score_pars.thf= pair[1];
		}
		else if(pair[0] == "SUMF"){
			if(pair[1]=="mean" || pair[1]=="median" || pair[1]=="max"){
				score_pars.sumf1 = pair[1];
				score_pars.sumf2 = pair[1];
			}
			else{
				cerr<<"ERROR: invalid SUMF: "<<pair[1]<<"\n";
				exit(1);
			}
		}
		else if(pair[0] == "SUMF1"){
			if(pair[1]=="mean" || pair[1]=="median" || pair[1]=="max"){
				score_pars.sumf1 = pair[1];
			}
			else{
				cerr<<"ERROR: invalid SUMF1: "<<pair[1]<<"\n";
				exit(1);
			}
		}
		else if(pair[0] == "SUMF2"){
			if(pair[1]=="mean" || pair[1]=="median" || pair[1]=="max"){
				score_pars.sumf2 = pair[1];
			}
			else{
				cerr<<"ERROR: invalid SUMF2: "<<pair[1]<<"\n";
				exit(1);
			}
		}			
		else if(pair[0] == "SF" && (pair[1]=="U"
						|| pair[1]=="U1"
						|| pair[1]=="U2"
						|| pair[1]=="U3"
						|| pair[1]=="AUC"							
						|| pair[1]=="AUC1"
						|| pair[1]=="AUC2"
						|| pair[1]=="AUC3"
						|| pair[1]=="AUCPR"
						|| pair[1]=="Jacc"
						|| pair[1]=="wJacc"
						|| pair[1]=="Fmax"
						|| pair[1]=="Smin"
						|| pair[1]=="Smin1"
						|| pair[1]=="Smin2"
						|| pair[1]=="Smin3"
						|| pair[1]=="SimUI"
						|| pair[1]=="SimGIC"
						|| pair[1]=="SimGIC2"
						|| pair[1]=="SminAll"
						|| pair[1]== "score_A"
						|| pair[1]== "score_B"
						|| pair[1]== "score_C"
						|| pair[1]== "score_D"
						|| pair[1]== "score_E"
						|| pair[1]== "score_F")){
			score_pars.scoref= pair[1];
		}
		else if(pair[0] == "SIMF" && (pair[1]==   "pJacc" ||
						pair[1]== "Resnik"||
						pair[1]== "Lin" ||
						pair[1]== "PathLin" ||
						pair[1]== "JC" ||
						pair[1]== "id" ||
						pair[1]== "NULL")){
			score_pars.simf= pair[1];
		}
		else if(pair[0] == "MINCS"){
			score_pars.minClassSize = atoi(pair[1].c_str());
		}
		else if(pair[0] == "GOC"){
				score_pars.goclass_count = atoi(pair[1].c_str());
		}		
		else{
			cerr << "ERROR: invalid -m argument:\""<<pair_list[i]<<"\"\n";
			exit(1);
		}
	}
	
	// SANITY CHECK
	if(pars.goa_pred == NULL){
		cerr << "ERROR: missing -p goa_pred\n\n";
		exit(1);
	}
	if(pars.goa_pos == NULL){
		cerr << "ERROR: missing -t goa_pred\n\n";
		exit(1);
	}
	if(pars.ic_file==NULL){
		if(score_pars.simf=="Resnik" 
			|| score_pars.simf=="Lin" 
			|| score_pars.simf=="PathLin" 
			|| score_pars.simf=="JC"){
			cerr << "ERROR: \"SIMF="<<score_pars.simf<<"\" option requires -i ic_file\n";
			exit(1);
		}
		if(score_pars.scoref.find("Smin")==0 || score_pars.scoref.find("SimGIC")==0){	// Smin-cluster metric
			cerr << "ERROR: \"SF="<<score_pars.scoref<<"\" option requires -i ic_file\n";
			exit(1);
		}
	}
	if(pars.obo_file == NULL){
		if(score_pars.simf !=  "NULL"){ 
			cerr<< "ERROR: \"-m SIMF\" option requires -b obo_file\n";
			exit(1);
		}
		if(pars.propagate_parents){
			cerr<< "ERROR: \"-g\" option requires -b obo_file\n";
			exit(1);
		}
	}
	
	// DEBUG
	if(pars.verbal){
		cout << "# score parameters:\n";
		print_score_parameters(score_pars);
	}
	

	generate_scores(pars,score_pars);
	exit(0);
}
	
