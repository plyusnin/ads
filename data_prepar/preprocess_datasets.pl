#
#	ADS PROJECT: PREPROCESSING DATASETS
#	Author: Ilja.Pljusnin@helsinki.fi
#
###############################################


# DATA: UniProtKB: GOA: loaded 2019.01.18
cd /data/ilja/respos/ads/data
wget ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/UNIPROT/goa_uniprot_all.gaf.gz
wget http://purl.obolibrary.org/obo/go.obo
gunzip goa_uniprot_all.gaf.gz

# FILTER IEA and ISS field 7
cat goa_uniprot_all.gaf | grep -vP "\tIEA\t" | grep -vP "\tISS\t" | grep -vP "^\!" 1> goa_uniprot_flt.gaf &
grep -cP "\tIEA\t" goa_uniprot_all.gaf
grep -cP "\tISS\t" goa_uniprot_all.gaf

# GO PARENT MAP
../bin/gorelatives -b go.obo -q all -r isa,partof,consider,replacedby,altid -d parents -v > go.parents
cat goa_uniprot_flt.gaf | cut -f5 | sort | uniq | cut -d: -f2 > uniprot.goids & # (26 406 ids)

# ESTIMATE INFORMATION CONTENT
perl ../perl/GOcounter.pl goa_uniprot_flt.gaf go.parents > uniprot.gocounts
cut -f1,4 uniprot.gocounts > uniprot.ic
../bin/propagate_ic -i uniprot.ic -o uniprot.ext.ic -b go.obo -v # extent IC to neighbors that have no IC defined

# FILTER UNINFORMATIVE large GO TERMS WITH FREQ > 0.2
../perl/filter_goa2.pl goa_uniprot_flt.gaf uniprot.gocounts 1> goa_uniprot_flt2.gaf

# SELECT GENE AND GO FIELDS
cat goa_uniprot_flt2.gaf | cut -f2,5 | sort | uniq > uniprot.genego

# SAMPLE BY GENE
../perl/sample_by_gene.pl uniprot.genego uniprot.1000.genego 1000 1

# DATA: mouseFunc (2006)
wget http://hugheslab.med.utoronto.ca/supplementary-data/mouseFunc_I/MouseFunc_Key.zip
mkdir mousefunc
unzip MouseFunc_Key.zip mousefunc/
../perl/update_mousefunc.pl 1> mouseFunc.genego
	# no geneid-genename-link: 0
	# no annotation in UniProt: 316
	# gene annotated: 1638

# CAFA2012 - Molecular Function + Biological Process
# Radivojac 2013: "A large-scale evaluation of computational protein function prediction": Supplementary Table 1 (98K):
# http://www.nature.com/nmeth/journal/v10/n3/extref/nmeth.2340-S2.xlsx
# download UniProt idmapping, create a two-column map
cd /data/ilja/data/uniprot/
wget ftp://ftp.ebi.ac.uk/pub/databases/uniprot/current_release/knowledgebase/idmapping/idmapping.dat.gz
gunzip idmapping_selected.tab.gz
cat idmapping_selected.tab | awk -F"\t" -v OFS="\t"  '{print $2,$1}' 1> idmapping_selected_f2f1.tab

# download CAFA2012 S2 Table (http://www.nature.com/nmeth/journal/v10/n3/extref/nmeth.2340-S2.xlsx), export annotations to cafa2012_bioproc.txt & cafa2012_molfunc.txt
cd /data/ilja/repos/ads/data/
cat cafa2012_bioproc.txt cafa2012_molfunc.txt | cut -f1 | sort | uniq > cafa2012.gene
../bin/linkids -d cafa2012.gene -c 1 -m /data/ilja/data/uniprot/idmapping_selected_f2f1.tab -v 1> cafa2012.gene2 2> cafa2012.gene2.log &
	# summary: linked 826/lines 867, nomatch 41, noid 0
../perl/update_cafa2012.pl 1> cafa2012.genego 2> cafa2012.log &
	# genes annotated: 825/826 (99.88%)
