#!/usr/bin/perl
use strict;
use warnings;

#
# Scores False Positive Set (FPS) with scoring options parsed from elab file(s).
#

my $usage= 	"\nUSAGE: $0 goscores fps[,fps2] elab_file1 [elab_file2..]\n".
		"goscores    \t: path to goscores utility\n".
		"fps         \t: false positive set (i.e. 800 largest GOs for each gene)\n".
		"            \t  or a list of FPS sets separated by commas\n".
		"elab_file   \t: scores from an elab run\n".
		"            \t: when several fps and elab files are suplied prints a table\n\n";

if(scalar(@ARGV)<3) { die $usage; }

my $goscores	= shift(@ARGV);
my $fps		= shift(@ARGV);
my @fps_list	= split(/,/,$fps);
my @elab_files	= @ARGV;
my $tmp_file	= "out.tmp";


# print headers
print "metric";
for(my $i=0; $i<scalar(@fps_list); $i++){
	my $label = $fps_list[$i];
	$label =~ s/.+\///g;
	print "\t$label";
}print "\n";


foreach my $elab_file(@elab_files){
  my $pos_file;
  my $obo_file;
  my $ic_file = "";
  my $mode;
  my $scoref;
  my $prop_par= "";
  my @error_levels;
  my @score_mat;
  my $ncol;
  my $nrow;
  my $fps_score; 

  open(IN,"<$elab_file") or die "Can\'t open $elab_file: $!\n";
  my $linen=0;
  while(my $l=<IN>){
	$linen++;
	chomp($l);
	if ( $l =~ m/^[\!#]/){
		$l =~ s/^[\!#]//;
		
		if($l =~ m/goa_pos\:\s*([\w\/\-\.]+)/){
			$pos_file= $1;
		}
		elsif($l =~ m/obo_file\:\s*([\w\/\-\.]+)/){
			$obo_file= $1;
		}
		elsif($l =~ m/ic_file\:\s*([\w\/\-\.]+)/){
			$ic_file= "-i $1"; # this will be "-i ic_file" or "" empty
		}
		elsif($l =~ m/mode\:\s*([\w=,]+)/){
			$mode= $1;
		}
		elsif($l =~ m/prop_par\:\s*[Tt]/){
			$prop_par= "-g";
		}
	}
	else{
		last;
	}
  }
  close(IN);
  
  # SCORING FUNCTION
  my @temp= split(/,/,$mode);
  for(my $i=0; $i<=$#temp; $i++){
  	my @pair= split(/=/,$temp[$i]);
  	if($pair[0] eq "SF"){
		$scoref= $pair[1];
		last;
	}
  }

  # SCORE FPS
  $elab_file =~ s/.+\///g; # remove path prefix
  $elab_file =~ s/\.\w+$//; # remove .* suffix
  print $elab_file;
  
  foreach $fps(@fps_list){
  	my $call = "$goscores -p $fps -t $pos_file -b $obo_file $ic_file $prop_par -m $mode 1> $tmp_file";
  	if(system($call) != 0){
		print STDERR "ERROR: failed to execute: $call\n";
		exit(1);
 	 }
  	open(IN,"<$tmp_file") or die "Can\'t open $tmp_file: $!\n";
  	my $l=<IN>; chomp($l);
  	$fps_score= $l;
  	close(IN);
  	print "\t$fps_score";
  }
  print "\n";
}
