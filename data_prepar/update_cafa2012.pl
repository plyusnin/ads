#!/usr/bin/perl
use strict;
use warnings;

# Links genes in cafa2012 (CAFA I) dataset to GO terms extracted from UniProt (2019-01-16)
# Prints summary to STDERR: how many genes were re-annotated using UniProt, how many had no annotations.
# Prints <geneid\tgo> file to STDOUT.
#
# USAGE $0 1> cafa2012_updated.genego 2> cafa2012_updated.log

my $root	= "/data/ilja/repos/ads/";
my $f1		= "$root/data/cafa2012.gene2"; # this MUST have columns: col1= original cafaI genids, col2= corresponding UniProtKB accession ids
my $f2		= "$root/data/goa_uniprot_flt.gaf";
my $CELLULAR_COMPONENT= 5575;
my $BIOLOGICAL_PROCESS= 8150;
my $MOLECULAR_FUNCTION= 3674;


my %geneid_go;
read_tohash_multiple($f2,2-1,5-1,\%geneid_go);

# removing "GO:" prefixes
foreach my $v (values %geneid_go){
	my @gos= split(/,/,$v,-1);
	foreach my $go(@gos){ $go=~ s/GO://;}
	$v = join(",",@gos);
}
# removing root nodes:
foreach my $v(values %geneid_go){
	my @gos= split(/,/,$v,-1);
	my @flt= ();
	foreach my $go(@gos){
		if(($go != $CELLULAR_COMPONENT) && ($go != $BIOLOGICAL_PROCESS) && ($go!= $MOLECULAR_FUNCTION)){
			push(@flt,$go);
		}
	}
	$v = join(",",@flt);
}

# process CAFA genes and print new annotation from UniProt (if available)
my $count_genes 	= 0;
my $count_noannot 	= 0;
my $count_annot		= 0;
open(IN,"<$f1") or die "Can\'t open $f1: $!\n";
while(my $l=<IN>){
	chomp($l);
	$count_genes++;
	my @a= split('\t',$l,-1);
	my $geneid= $a[1];

	if(!defined($geneid_go{$geneid})){
		$count_noannot++;
		print STDERR "WARNING: no annotation for gene: $geneid\n";
		next;
	}
	
	my @gos= split(",",$geneid_go{$geneid});
	foreach my $go(@gos){
		print "$geneid\t$go\n";
	}
	$count_annot++;
}
printf STDERR ("# genes no annot: %u/%u (%.2f%%)\n",$count_noannot,$count_genes, ($count_noannot/$count_genes*100));
printf STDERR ("# genes annotated: %u/%u (%.2f%%)\n",$count_annot,$count_genes,  ($count_annot/$count_genes*100));


# read_hash(file,key_ind,val_ind,\%hash)
# if the same key points to multiple values, these are separated by comma
sub read_tohash_multiple{
	my $file= shift;
	my $keyi= shift;
	my $vali= shift;
	my $hashp= shift;
	print STDERR "# read_tohash from $file\n";
	open(IN,"<$file") or die "Can\'t open $file: $!\n";
	my $ln=0;
	while(my $l=<IN>){
		if($l =~ "^[!#]"){
			next;
		}
        	$ln++; if($ln%50000000 == 0){print STDERR "\t $ln lines read\n"};
		chomp($l);
		my @sp= split(/\t/,$l,-1);
		if(defined($hashp->{$sp[$keyi]})){
			$hashp->{$sp[$keyi]} = join(",",$hashp->{$sp[$keyi]},$sp[$vali]);
		}
		else{
        		$hashp->{$sp[$keyi]} = $sp[$vali];
		}
	}
	close(IN);
	
	# remove possible duplicates
	foreach my $v(values %{$hashp}){
		my %tmp; 
		my @sp= split(",",$v,-1);
		foreach my $x(@sp){$tmp{$x} = 1;}
		$v = join(",", sort keys %tmp);
	}
}

