
all: elab goscores propagate_ic

elab: cpp/main.cpp
	g++ -Wall -O3 -std=c++0x -Icpp cpp/main.cpp -o bin/elab

goscores: cpp/goscores.cpp	
	g++ -Wall -O3 -std=c++0x -Icpp cpp/goscores.cpp -o bin/goscores
	
gorelatives: cpp/gorelatives.cpp
	g++ -Wall -O3 -std=c++0x -Icpp cpp/gorelatives.cpp -o bin/gorelatives

propagate_ic: cpp/propagate_ic.cpp
	g++ -Wall -O3 -std=c++0x -Icpp cpp/propagate_ic.cpp -o bin/propagate_ic
	
tests: cpp/tests.cpp
	g++ -Wall -std=c++0x -Icpp cpp/tests.cpp  -o bin/tests
	
#clean:
#	rm *.o
